# *NOTICE* - this repo is now being maintained on github [here](https://github.com/small-hack/argocd/tree/main/nextcloud) 


# ArgoCD repo for deploying apps for NextCloud on K8s

This repo will help you install the following helm charts and CRDs:

- NextCloud
- K8up
- Prometheus + AlertManager + Grafana

## Quickstart (already have a cluster with ArgoCD installed)
Assuming you have this repo cloned locally, make sure you're logged in:

```bash
argocd login --username admin --grpc-web your-argocd-domain.tld
```

Then proceed to deploy the app:

```bash
argocd app create app-of-apps -f app_of_apps.yaml
```

And you should be done, but if you want to sync the app manually from cli:

```bash
# sync the base app
argocd app sync apps
```

That's it :)

## Additional notes on ArgoCD
If the repo you're using with argo is private, you'll need to create repo credentials. To do that in a declarative way, for gitlab, you first need to [create a deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html), and then you can create a secret that looks like this:

```yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: private-repo-creds
  namespace: argocd
  labels:
    argocd.argoproj.io/secret-type: repo-creds
stringData:
  type: git
  url: https://gitlab.com/vleermuis_tech/goobernetes/argocd
  password: replace-with-deploy-token-from-your-personal-gitlab-project
  username: username-from-your-deploy-token
```
To create repo credentials via the cli, check out [this guide](https://argo-cd.readthedocs.io/en/stable/user-guide/commands/argocd_repocreds_add/).


## ESO (External Secret Operator)

I use the external secrets operator with gitlab for my projects with secrets that I need to manage.

You can learn more about ESO here:
https://external-secrets.io

And learn more about the gitlab provider specificly here:
https://external-secrets.io/provider-gitlab-project-variables/


## Creating an Argo CD app of apps

Ref for basic app of apps:
https://www.arthurkoziel.com/setting-up-argocd-with-helm/

This explains sync waves, which are used for determining which app gets synced first:
(lower number is deployed first)
https://argo-cd.readthedocs.io/en/stable/user-guide/sync-waves/

```bash
# this creates the core app that minds the other apps
argocd app create  --dest-namespace argocd \
                   --dest-server https://kubernetes.default.svc \
                   --repo https://gitlab.com/your-argo-repo.git \
                   --path templates

# this syncs any apps that don't have auto-sync on :)
argocd app sync apps
```
